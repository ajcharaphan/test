pipeline {
	agent any
		
    tools {
        go 'Go'
    }

    stages {
	    stage('Prepare') {
            steps {
				script {
					def props = readProperties file: '.env'
                    //props.each{ k,v -> println "$k = $v" }
                    props.each{ k,v -> env."${k}" = "${v}" }
					//MESSAGE_TEMPLATE = "$JOB_NAME Build #$BUILD_ID " + env['MESSAGE_TEMPLATE'] + "\r\n ${BUILD_URL}input"
				
					print '### Kill Go ###'
					sh '/bin/kill -HUP $(pidof maingobuild) || true'
					
					print '### Build Go Step ###'
					sh 'go build'
					sh 'ls -lt'
					
					// print '### Config Permission Jenkins Step ###'
					// sh 'chmod 666 /var/run/docker.sock'
				}
            }
        }

        stage('Artifactory') {
            steps {
				script {
					withCredentials([
						usernamePassword(credentialsId: 'artifactory',
						usernameVariable: 'username',
						passwordVariable: 'password')
					]) {
	
					print '### Config Credential Step ###'
					sh 'jfrog rt config --url=${ARTIFACTORY_URL} --user=$username --password=$password'
					
					print '### Push to Artifactory Step ###'
					sh 'jfrog rt u maingobuild ${ARTIFACTORY_REPO}/maingobuild'
					}
				}
			}
        }
		
        stage('Deploy') {
            steps {
				script {
					print '### Pull From Artifactory Step ###'
					sh 'jfrog rt dl ${ARTIFACTORY_REPO}/maingobuild devops_robot/'
					
					print '### Run Executable Go Step ###'
					sh 'chmod +x ./devops_robot/maingobuild'
					sh 'nohup ./devops_robot/maingobuild &'
				}
			}
		}
		
		stage('Test') {
            steps {
				script {
					print '### Launch Go Test Step ###'
					sh 'curl http://${HOSTNAME}:${PORT}/'

					print '### Robot Test Step ###'
					sh 'robot -d devops_robot/result devops_robot/script/'
					
					print '### Jmeter Test Step ###'
					sh '/opt/apache-jmeter/bin/jmeter -n -t ./devops_jmeter/script/${JMETER_FILE} -l ./devops_jmeter/result/jmeter_log.jtl'
				}
			}
			post {
				always {
					script {
						dir("./devops_robot/result"){
							if(fileExists("/")){
								step(
									[
									  $class              : 'RobotPublisher',
									  outputPath          : '',
									  outputFileName      : '**/output.xml',
									  reportFileName      : '**/report.html',
									  logFileName         : '**/log.html',
									  disableArchiveOutput: false,
									  passThreshold       : 50,
									  unstableThreshold   : 40,
									  otherFiles          : "**/*.png,**/*.jpg",
									]
								)
							}
						}
						
						dir("./devops_jmeter/result"){
							if(fileExists("/")){
								// Publish JMeter Report
								perfReport "**/*.jtl"
							}
						}
					}
				}
			}
		}

//        stage('Build Image') {
//            steps {
//                print 'Build Image To Docker Step'
//				sh 'docker build -t imgmain:v1.0.0 .'
//				sh 'docker run -it -d --name containermain -p 18081:18080 imgmain:v1.0.0'
//            }
//        }
//
//        stage('Test Image') {
//            steps {
//                print 'Testing... Step'
//				sh 'curl http://devopsserver:18081/'
//            }
//        }
//		
//        stage('Push To Harbor') {
//            steps {
//				script {
//					withCredentials([
//						usernamePassword(credentialsId: 'harbor',
//						usernameVariable: 'username',
//						passwordVariable: 'password')
//					]) {
//					// print 'username=' + username + 'password=' + password
//                
//					print 'Push To Harbor Step'
//					sh 'docker login devopsserver:9443 -u $username -p $password'
//					sh 'docker tag imgmain:v1.0.0 devopsserver:9443/devops/go:v1.0.0'
//					sh 'docker push devopsserver:9443/devops/go:v1.0.0'
//					sh 'docker logout devopsserver:9443'
//					sh 'docker rm containermain -f'
//					sh 'docker rmi devopsserver:9443/devops/go:v1.0.0'
//					sh 'docker rmi imgmain:v1.0.0'
//					}
//				}
//            }
//        }
			
		stage('SonarQube Analysis') {
            steps{
                script{
					print '### SonarQube Analysis Step ###'
                    def scannerHome = tool 'sonar-scanner';
                    withSonarQubeEnv('sonar') {
                        sh "${scannerHome}/bin/sonar-scanner"
						//sh "${scannerPath}/sonar-scanner"
                    }
                }
            }                            
        }
        
        stage('Quality Gate') {
            steps {
				print '### Quality Gate Step ###'
                timeout(time: 1, unit: 'HOURS') {
                    // Parameter indicates whether to set pipeline to UNSTABLE if Quality Gate fails
                    // true = set pipeline to UNSTABLE, false = don't
                    waitForQualityGate abortPipeline: true
                }
            }
        }
    }
}