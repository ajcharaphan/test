FROM golang:1.14

# Set necessary environmet variables needed for our image
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

# Move to /dist directory as the place for resulting binary folder
WORKDIR /go/build

# Copy binary from build to main folder
COPY . .

# Export necessary port
#EXPOSE 18080

# Command to run when starting the container
CMD ["/go/build/maingobuild"]

