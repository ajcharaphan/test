package main

import (
   "encoding/json"
   "fmt"
   "net/http" // import package net-http
)

type addressBook struct {
    Firstname string
    Lastname  string
    Code      int
    Phone     string
}

func getAddressBookAll(w http.ResponseWriter, r *http.Request) {
   addBook := addressBook{
                Firstname: "Ajcharaphan",
                Lastname:  "Matvijit",
                Code:      1234,
                Phone:     "0800000000",
              }
   json.NewEncoder(w).Encode(addBook) // (1)
}

func homePage(w http.ResponseWriter, r *http.Request) { // (1)
    fmt.Fprint(w, "Welcome to the HomePage!") // (2)
}

func handleRequest() { // (3)
    http.HandleFunc("/", homePage) // (4)
    http.HandleFunc("/getAddress", getAddressBookAll)
    http.ListenAndServe(":18080", nil) // (5)
}

func main() {
   //fmt.Println("Create File Main.go Success")
   handleRequest()
}
