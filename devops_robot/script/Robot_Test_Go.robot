*** Setting ***
Library    RequestsLibrary
Library    Collections

*** Variables ***
${host}    devopsserver
${port}    18080
${config_protocal}    http
${uri}    /getAddress

*** Keywords ***
Verify Json API
    ${url}=    Set Variable    ${config_protocal}://${host}:${port}
    Create Session    devops    ${url}
    ${response}=    Get Request    devops    ${uri}
    Should Be Equal As Strings    ${response.status_code}    200
    ${json}    To Json    ${response.content}
	Dictionary Should Contain Item    ${json}    Firstname    Ajcharaphan
	Dictionary Should Contain Item    ${json}    Lastname    Matvijit
	Dictionary Should Contain Item    ${json}    Code    1234
	Dictionary Should Contain Item    ${json}    Phone    0800000000

Verify Text API
    ${url}=    Set Variable    ${config_protocal}://${host}:${port}
    Create Session    devops    ${url}
    ${response}=    Get Request    devops    /
    Should Be Equal As Strings    ${response.status_code}    200
	Should Be Equal As Strings    ${response.content}    Welcome to the HomePage!


*** Test Cases ***
TC01 Test Go API
    Verify Json API

TC02 Test Go Text
	Verify Text API


